﻿using System.Collections.Generic;
using NBitcoin;
using System.Linq;
using NBitcoin.Crypto;
using NBitcoin.Indexer;
using QBitNinja.Models;
using System;
using System.Text;
using NBitcoin.DataEncoders;
using System.Threading;
using System.Threading.Tasks;

namespace QBitNinja
{
    public class WalletRepository
    {
        public WalletRepository(IndexerClient indexer,
                                Func<Scope, ChainTable<Models.BalanceSummary>> getBalancesCacheTable,
                                CrudTableFactory tableFactory)
        {
            if(indexer == null)
                throw new ArgumentNullException("indexer");
            if(tableFactory == null)
                throw new ArgumentNullException("tableFactory");
            if(getBalancesCacheTable == null)
                throw new ArgumentNullException("getBalancesCacheTable");
            GetBalancesCacheTable = getBalancesCacheTable;
            _walletAddressesTable = tableFactory.GetTable<WalletAddress>("wa");
            WalletTable = tableFactory.GetTable<WalletModel>("wm");
            KeySetTable = tableFactory.GetTable<KeySetData>("ks");
            _keyDataTable = tableFactory.GetTable<HDKeyData>("kd");
            Indexer = indexer;
        }

        Func<Scope, ChainTable<Models.BalanceSummary>> GetBalancesCacheTable;


        private readonly CrudTable<WalletAddress> _walletAddressesTable;
        public CrudTable<WalletAddress> WalletAddressesTable
        {
            get
            {
                return _walletAddressesTable;
            }
        }

        private readonly CrudTable<HDKeyData> _keyDataTable;
        public CrudTable<HDKeyData> KeyDataTable
        {
            get
            {
                return _keyDataTable;
            }
        }

        public CrudTable<KeySetData> KeySetTable { get; }

        public CrudTable<WalletModel> WalletTable { get; }

        public IndexerClient Indexer { get; }

        public bool Create(WalletModel wallet)
        {
            return WalletTable.Create(wallet.Name, wallet, false);
        }

        public async Task<WalletModel> GetWallet(string walletName)
        {
            return await WalletTable.ReadOneAsync(walletName);
        }

        public async Task<IEnumerable<WalletModel>> GetAsync()
        {
            return await WalletTable.ReadAsync();
        }

        private bool AddAddress(WalletAddress address)
        {
            if(address.Address == null)
                throw new ArgumentException("Address should not be null", "address.Address");

            return WalletAddressesTable
                .GetChild(address.WalletName)
                .Create(address.Address.ToString(), address, false);
        }

        public async Task<List<WalletAddress>> Scan(string walletName, KeySetData keysetData, int from, int lookahead)
        {
            List<WalletAddress> addedAddresses = new List<WalletAddress>();
            bool nextUnusedChanged = false;
            int nextToScan = -1;
            while(true)
            {
                List<uint> used = new List<uint>();
                var start = nextToScan == -1 ? from : nextToScan;
                var count = nextToScan == -1 ? lookahead : lookahead - (nextToScan - keysetData.State.NextUnused);
                var addresses = keysetData.KeySet.GetKeys(start)
                                          .Take(count)
                                          .Select(key => WalletAddress.ToWalletAddress(walletName, keysetData, key))
                                          .ToArray();
                nextToScan = from + lookahead;
                HackToPreventOOM(addresses);
                foreach (var address in addresses)
                {
                    var addWalletResult = await AddWalletAddress(address, true);
                    if (addWalletResult.Empty)
                    {
                        var n = address.HDKey.Path.Indexes.Last();
                        if(!addWalletResult.Empty)
                        {
                            lock(used)
                                used.Add(n);
                        }
                    }
                }
                addedAddresses.AddRange(addresses);
                if(used.Count == 0)
                    break;

                keysetData.State.NextUnused = (int)(used.Max() + 1);
                nextUnusedChanged = true;
            }
            if(nextUnusedChanged)
                SetKeySet(walletName, keysetData);
            return addedAddresses;
        }
        private static void HackToPreventOOM(WalletAddress[] addresses)
        {
            var addr = addresses.FirstOrDefault();
            if(addr != null)
                addr.CreateWalletRuleEntry().CreateTableEntity();
        }


        private static string Hash(WalletAddress address)
        {
            return Hashes.Hash256(Encoding.UTF8.GetBytes(Serializer.ToString(address))).ToString();
        }

        public async Task<IEnumerable<WalletAddress>> GetAddressesAsync(string walletName)
        {
            return await WalletAddressesTable.GetChild(walletName).ReadAsync();
        }

        public bool AddKeySet(string walletName, KeySetData keysetData)
        {
            return KeySetTable.GetChild(walletName).Create(keysetData.KeySet.Name, keysetData, false);
        }
        public bool SetKeySet(string walletName, KeySetData keysetData)
        {
            return KeySetTable.GetChild(walletName).Create(keysetData.KeySet.Name, keysetData, true);
        }

        public async Task<bool> DeleteKeySet(string walletName, string keyset)
        {
            if(!await KeySetTable.GetChild(walletName).DeleteAsync(keyset, true))
                return false;

            await KeyDataTable.GetChild(walletName, keyset).DeleteAsync();
            return true;
        }

        public async Task<KeySetData> GetKeySetDataAsync(string walletName, string keysetName)
        {
            return await KeySetTable.GetChild(walletName).ReadOneAsync(keysetName);
        }

        private static string Encode(Script script)
        {
            return Encoders.Hex.EncodeData(script.ToBytes(true));
        }

        public Network Network
        {
            get
            {
                return Indexer.Configuration.Network;
            }
        }

        private static KeyPath Inc(KeyPath keyPath)
        {
            var indexes = keyPath.Indexes.ToArray();
            indexes[indexes.Length - 1] = indexes[indexes.Length - 1] + 1;
            return new KeyPath(indexes);
        }

        public async Task<IEnumerable<KeySetData>> GetKeysetsAsync(string walletName)
        {
            return await KeySetTable.GetChild(walletName).ReadAsync();
        }

        internal async Task<IEnumerable<HDKeyData>> GetKeysAsync(string walletName, string keysetName)
        {
            return await KeyDataTable.GetChild(walletName, keysetName).ReadAsync();
        }
        public async Task<(bool Added, bool Empty)> AddWalletAddress(WalletAddress address, bool mergePast)
        {
            var empty = true;
            if(!AddAddress(address))
                return (false, empty);

            if(address.HDKeySet != null)
                KeyDataTable.GetChild(address.WalletName, address.HDKeySet.Name).Create(Encode(address.ScriptPubKey), address.HDKey, false);

            var entry = address.CreateWalletRuleEntry();
            Indexer.AddWalletRule(entry.WalletId, entry.Rule);
            if(mergePast)
            {
                CancellationTokenSource cancel = new CancellationTokenSource();
                cancel.CancelAfter(10000);
                empty = !await Indexer.MergeIntoWallet(address.WalletName, address, entry.Rule, cancel.Token);
            }
            if(!empty)
            {
                GetBalanceSummaryCacheTable(address.WalletName, true).Delete();
                GetBalanceSummaryCacheTable(address.WalletName, false).Delete();
            }
            return (true, empty);
        }


        public ChainTable<Models.BalanceSummary> GetBalanceSummaryCacheTable(string walletName, bool colored)
        {
            var balanceId = new BalanceId(walletName);
            return GetBalanceSummaryCacheTable(balanceId, colored);
        }

        public ChainTable<Models.BalanceSummary> GetBalanceSummaryCacheTable(BalanceId balanceId, bool colored)
        {
            Scope scope = new Scope(new[] { balanceId.ToString() });
            scope = scope.GetChild(colored ? "colsum" : "balsum");
            var cacheTable = GetBalancesCacheTable(scope);
            return cacheTable;
        }        
    }
}
