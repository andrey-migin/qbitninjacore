using NBitcoin;
using NBitcoin.Crypto;
using QBitNinja.Models;
using QBitNinja.Notifications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QBitNinja
{
	public class QBitTopics
	{
		public QBitTopics(QBitNinjaConfiguration configuration, string uniquePrefix)
		{
			BroadcastedTransactions = new QBitNinjaTopic<BroadcastedTransaction>(configuration.ServiceBus,
				new TopicCreation(configuration.Indexer.GetTable("broadcastedtransactions").Name)
				{
					EnableExpress = true
				},uniquePrefix,
				new SubscriptionCreation
				{
					AutoDeleteOnIdle = TimeSpan.FromHours(24.0)
				});

			AddedAddresses = new QBitNinjaTopic<WalletAddress[]>(configuration.ServiceBus,
				new TopicCreation(configuration.Indexer.GetTable("walletrules").Name)
				{
					EnableExpress = true,
					DefaultMessageTimeToLive = TimeSpan.FromMinutes(5.0)
				}, 
				uniquePrefix
				,new SubscriptionCreation()
				{
					AutoDeleteOnIdle = TimeSpan.FromHours(24.0)
				});

			NewBlocks = new QBitNinjaTopic<BlockHeader>(configuration.ServiceBus,
				new TopicCreation(configuration.Indexer.GetTable("newblocks").Name)
				{
					DefaultMessageTimeToLive = TimeSpan.FromMinutes(5.0),
					EnableExpress = true
				},
				uniquePrefix,
				new SubscriptionCreation()
				{
					AutoDeleteOnIdle = TimeSpan.FromHours(24.0)
				});

			NewTransactions = new QBitNinjaTopic<Transaction>(configuration.ServiceBus,
				new TopicCreation(configuration.Indexer.GetTable("newtransactions").Name)
				{
					DefaultMessageTimeToLive = TimeSpan.FromMinutes(5.0),
				},
				uniquePrefix,				
				new SubscriptionCreation()
				{
					AutoDeleteOnIdle = TimeSpan.FromHours(24.0),
				});

			SubscriptionChanges = new QBitNinjaTopic<SubscriptionChange>(configuration.ServiceBus,
				new TopicCreation(configuration.Indexer.GetTable("subscriptionchanges").Name)
				{
					DefaultMessageTimeToLive = TimeSpan.FromMinutes(5.0),
					EnableExpress = true
				}, uniquePrefix);

			_sendNotifications = new QBitNinjaQueue<Notify>(configuration.ServiceBus,
				new QueueCreation(configuration.Indexer.GetTable("sendnotifications").Name)
				{
					RequiresDuplicateDetection = true,
					DuplicateDetectionHistoryTimeWindow = TimeSpan.FromMinutes(10.0),
				});
			_sendNotifications.GetMessageId = (n) =>
				Hashes.Hash256(Encoding.UTF32.GetBytes(n.Notification.ToString())).ToString();


			InitialIndexing = new QBitNinjaQueue<BlockRange>(configuration.ServiceBus,
				new QueueCreation(configuration.Indexer.GetTable("intitialindexing").Name)
				{
					RequiresDuplicateDetection = true,
					DuplicateDetectionHistoryTimeWindow = TimeSpan.FromMinutes(10.0),
					MaxDeliveryCount = int.MaxValue,
					LockDuration = TimeSpan.FromMinutes(5.0)
				}) {GetMessageId = (n) => n.ToString()};
		}


		public QBitNinjaQueue<BlockRange> InitialIndexing { get; }

		private readonly QBitNinjaQueue<Notify> _sendNotifications;

		public QBitNinjaQueue<Notify> SendNotifications => _sendNotifications;

		public QBitNinjaTopic<Transaction> NewTransactions { get; }

		public QBitNinjaTopic<BlockHeader> NewBlocks { get; }

		public QBitNinjaTopic<SubscriptionChange> SubscriptionChanges { get; }

		public QBitNinjaTopic<BroadcastedTransaction> BroadcastedTransactions { get; }

		public QBitNinjaTopic<WalletAddress[]> AddedAddresses { get; }


		internal async Task EnsureSetupAsync()
		{
			await BroadcastedTransactions.EnsureExistsAsync();
			await NewTransactions.EnsureExistsAsync();
			await NewBlocks.EnsureExistsAsync();
			await AddedAddresses.EnsureExistsAsync();
			await SubscriptionChanges.EnsureExistsAsync();
			await SendNotifications.EnsureExistsAsync();
			await InitialIndexing.EnsureExistsAsync();
			
		}
	}
}