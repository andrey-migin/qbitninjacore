﻿using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using NBitcoin;
using NBitcoin.DataEncoders;
using NBitcoin.Indexer;
using NBitcoin.Indexer.IndexTasks;
using QBitNinja.Notifications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.ExceptionServices;
using System.Threading;
using System.Threading.Tasks;

namespace QBitNinja
{
    public class BlockRange
    {
        public string Target
        {
            get;
            set;
        }
        public int From
        {
            get;
            set;
        }
        public int Count
        {
            get;
            set;
        }
        public bool Processed
        {
            get;
            set;
        }
        public override string ToString()
        {
            return Target + "- " + From + "-" + Count;
        }
    }


    public class InitialIndexer
    {

        QBitNinjaConfiguration _Conf;
        public InitialIndexer(QBitNinjaConfiguration conf)
        {
            if (conf == null)
                throw new ArgumentNullException("conf");
            _Conf = conf;
            BlockGranularity = 100;
            TransactionsPerWork = 1000 * 1000;
            Init();
        }

        public int BlockGranularity
        {
            get;
            set;
        }
        public int TransactionsPerWork
        {
            get;
            set;
        }



        private void Init()
        {
            var indexer = _Conf.Indexer.CreateIndexer();
            AddTaskIndex(indexer.GetCheckpoint(IndexerCheckpoints.Balances), new IndexBalanceTask(_Conf.Indexer, null));
            AddTaskIndex(indexer.GetCheckpoint(IndexerCheckpoints.Blocks), new IndexBlocksTask(_Conf.Indexer));
            AddTaskIndex(indexer.GetCheckpoint(IndexerCheckpoints.Transactions), new IndexTransactionsTask(_Conf.Indexer));
            AddTaskIndex(indexer.GetCheckpoint(IndexerCheckpoints.Wallets), new IndexBalanceTask(_Conf.Indexer, _Conf.Indexer.CreateIndexerClient().GetAllWalletRules()));
            var subscription = indexer.GetCheckpointRepository().GetCheckpoint("subscriptions");
            AddTaskIndex(subscription, new IndexNotificationsTask(_Conf, new SubscriptionCollection(_Conf.GetSubscriptionsTable().ReadAsync().Result)));
        }

        readonly Dictionary<string, Tuple<Checkpoint, IIndexTask>> _indexTasks = new Dictionary<string, Tuple<Checkpoint, IIndexTask>>();

        void AddTaskIndex(Checkpoint checkpoint, IIndexTask indexTask)
        {
            _indexTasks.Add(checkpoint.CheckpointName, Tuple.Create(checkpoint, indexTask));
        }


        public async Task CancelAsync()
        {
            var blobLock = GetInitBlob();
            try
            {
                try
                {
                    await blobLock.DeleteAsync();
                }
                catch
                {
                    await blobLock.BreakLeaseAsync(TimeSpan.FromSeconds(5));
                    await blobLock.DeleteAsync();
                }
            }
            catch (StorageException ex)
            {
                if (ex.RequestInformation == null || ex.RequestInformation.HttpStatusCode != 404)
                    throw;
            }
            ListenerTrace.Info("Init blob deleted");
            ListenerTrace.Info("Deleting queue...");
            await _Conf.Topics
                 .InitialIndexing
                 .GetNamespace().DeleteQueueAsync(_Conf.Topics.InitialIndexing.Queue);
            ListenerTrace.Info("Queue deleted");
        }

        private CloudBlockBlob GetInitBlob()
        {
            var container = _Conf.Indexer.GetBlocksContainer();
            var blobLock = container.GetBlockBlobReference("initialindexer/lock");
            return blobLock;
        }

        private void UpdateCheckpoints(BlockLocator locator)
        {
            ListenerTrace.Info("Work finished, updating checkpoints");
            foreach (var chk in _indexTasks.Select(kv => kv.Value.Item1))
            {
                ListenerTrace.Info(chk.CheckpointName + "...");
                chk.SaveProgress(locator);
            }
            ListenerTrace.Info("Checkpoints updated");
        }

        private async Task EnqueueJobsAsync(NodeBlocksRepository repo, ChainBase chain, CloudBlockBlob blobLock, string lease)
        {
            int cumul = 0;
            ChainedBlock from = chain.Genesis;
            int blockCount = 0;
            foreach (var block in repo.GetBlocks(new[] { chain.Genesis }.Concat(chain.EnumerateAfter(chain.Genesis)).Where(c => c.Height % BlockGranularity == 0).Select(c => c.HashBlock), default(CancellationToken)))
            {
                cumul += block.Transactions.Count * BlockGranularity;
                blockCount += BlockGranularity;
                if (cumul > TransactionsPerWork)
                {
                    var nextFrom = chain.GetBlock(chain.GetBlock(block.GetHash()).Height + BlockGranularity);
                    if (nextFrom == null)
                        break;
                    EnqueueRange(chain, from, blockCount);
                    from = nextFrom;
                    blockCount = 0;
                    cumul = 0;
                }
            }

            blockCount = (chain.Tip.Height - from.Height) + 1;
            EnqueueRange(chain, from, blockCount);

            var bytes = chain.Tip.GetLocator().ToBytes();
            await blobLock.UploadTextAsync(Encoders.Hex.EncodeData(bytes));
        }

        private void EnqueueRange(ChainBase chain, ChainedBlock startCumul, int blockCount)
        {
            ListenerTrace.Info("Enqueing from " + startCumul.Height + " " + blockCount + " blocks");
            if (blockCount == 0)
                return;
            var tasks = _indexTasks
                .Where(t => chain.FindFork(t.Value.Item1.BlockLocator).Height <= startCumul.Height + blockCount)
                .Select(t => new BlockRange()
                {
                    From = startCumul.Height,
                    Count = blockCount,
                    Target = t.Key
                })
                .Select(t => _Conf.Topics.InitialIndexing.AddAsync(t))
                .ToArray();

            try
            {
                Task.WaitAll(tasks);
            }
            catch (AggregateException aex)
            {
                ExceptionDispatchInfo.Capture(aex.InnerException).Throw();
                throw;
            }
        }

    }
}
