using System;

namespace QBitNinja
{
    public class MyActionDisposable : IDisposable
    {
    
        private readonly Action _onLeave;

        public MyActionDisposable(Action onLeave)
        {
            _onLeave = onLeave;
        }

        public void Dispose()
        {
            _onLeave();
        }
    }
}