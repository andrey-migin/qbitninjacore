﻿using NBitcoin;
using NBitcoin.DataEncoders;
using System;
using System.Runtime.ExceptionServices;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Azure.ServiceBus;
using Microsoft.Azure.ServiceBus.Management;

namespace QBitNinja.Notifications
{

    public class BrokeredMessage : IDisposable
    {
        public BrokeredMessage(string message)
        {
            TheMessage = new Message(Encoding.UTF8.GetBytes(message));
        }

        private readonly SubscriptionClient _subscriptionClient;
        public BrokeredMessage(Message theMessage, SubscriptionClient subscriptionClient)
        {
            TheMessage = theMessage;
            _subscriptionClient = subscriptionClient;
        }

        private readonly QueueClient _queueClient;
        public BrokeredMessage(Message theMessage, QueueClient queueClient)
        {
            TheMessage = theMessage;
            _queueClient = queueClient;
        }

        public Message TheMessage { get; }

        public string MessageId
        {
            get => TheMessage.MessageId;
            set => TheMessage.MessageId = value;
        }

        public DateTime ScheduledEnqueueTimeUtc
        {
            get => TheMessage.ScheduledEnqueueTimeUtc;
            set => TheMessage.ScheduledEnqueueTimeUtc = value;
        }

        public string BodyAsString()
        {
            return Encoding.UTF8.GetString(TheMessage.Body);
        }


        public async Task CompleteAsync()
        {
            if (_queueClient != null)
                await _queueClient.CompleteAsync(TheMessage.SystemProperties.LockToken);

            if (_subscriptionClient != null)
                await _subscriptionClient.CompleteAsync(TheMessage.SystemProperties.LockToken);
        }

        public void Dispose()
        {
            
        }
    }


    public abstract class QBitNinjaQueueBase<T, TCreation, TDescription>  where TCreation : ICreation
    {
        public QBitNinjaQueueBase(string connectionString, TCreation creation)
        {
            Creation = creation;
            ConnectionString = connectionString;
        }

        protected abstract Task SendAsync(BrokeredMessage message);
        protected abstract IDisposable OnMessageAsyncCore(Func<BrokeredMessage, Task> act, MessageHandlerOptions options);

    //    protected abstract Task<BrokeredMessage> ReceiveAsyncCore(TimeSpan timeout);

    protected string ConnectionString { get; }

    protected TCreation Creation { get; }

        public async Task<bool> AddAsync(T entity)
        {
            var str = Serializer.ToString(entity);
            BrokeredMessage brokered = new BrokeredMessage(str);
            if(Creation.RequiresDuplicateDetection.HasValue &&
                Creation.RequiresDuplicateDetection.Value)
            {
                if(GetMessageId == null)
                    throw new InvalidOperationException("Requires Duplicate Detection is on, but the callback GetMessageId is not set");
                brokered.MessageId = GetMessageId(entity);
            }

            using(brokered)
            {
                await SendAsync(brokered).ConfigureAwait(false);
            }
            return true;
        }

        protected void OnUnHandledException(Exception ex)
        {
            if(UnhandledException != null && ex != null)
                UnhandledException(ex);
        }

        public QBitNinjaQueueBase<T, TCreation, TDescription> AddUnhandledExceptionHandler(Action<Exception> handler)
        {
            UnhandledException += handler;
            return this;
        }

        public event Action<Exception> UnhandledException;

        public Func<T, string> GetMessageId
        {
            get;
            set;
        }

        public IDisposable OnMessage(Action<T> evt)
        {
            return OnMessage((a, b) => evt(a));
        }
        public IDisposable OnMessage(Action<T, MessageControl> evt, bool autoComplete = true)
        {
            return OnMessageAsync((a, b) =>
            {
                evt(a, b);
                return Task.FromResult(0);
            }, new MessageHandlerOptions(ExceptionReceivedHandler)
            {
                AutoComplete = autoComplete,
                MaxConcurrentCalls = 1,
            });
        }

        public IDisposable OnMessageAsync(Func<T, Task> evt)
        {
            return OnMessageAsync((a, b) => evt(a));
        }
        
        static Task ExceptionReceivedHandler(ExceptionReceivedEventArgs exceptionReceivedEventArgs)
        {
            Console.WriteLine($"Message handler encountered an exception {exceptionReceivedEventArgs.Exception}.");
            var context = exceptionReceivedEventArgs.ExceptionReceivedContext;
            Console.WriteLine("Exception context for troubleshooting:");
            Console.WriteLine($"- Endpoint: {context.Endpoint}");
            Console.WriteLine($"- Entity Path: {context.EntityPath}");
            Console.WriteLine($"- Executing Action: {context.Action}");
            return Task.CompletedTask;
        } 

        public IDisposable OnMessageAsync(Func<T, MessageControl, Task> evt, MessageHandlerOptions options = null)
        {
            if(options == null)
                options = new MessageHandlerOptions(ExceptionReceivedHandler)
                {
                    AutoComplete = true,
                    MaxConcurrentCalls = 10,
                   MaxAutoRenewDuration = TimeSpan.Zero
                };

            return OnMessageAsyncCore(async bm =>
            {
                var control = new MessageControl {Options = options};
                var obj = ToObject(bm);
                if(obj == null)
                    return;
                await evt(obj, control).ConfigureAwait(false);
                if(control.Scheduled != null)
                {
                    var message = new BrokeredMessage(Serializer.ToString(obj))
                    {
                        MessageId = Encoders.Hex.EncodeData(RandomUtils.GetBytes(32))
                    };
                    message.ScheduledEnqueueTimeUtc = control.Scheduled.Value;
                    await SendAsync(message).ConfigureAwait(false);
                    if(!options.AutoComplete)
                        if(control._Complete)
                        {
                            try
                            {
                                await bm.CompleteAsync().ConfigureAwait(false);
                            }
                            catch(ObjectDisposedException)
                            {
                                ListenerTrace.Error("Brokered message already disposed", null);
                            }
                        }
                }
            }, options);
        }

        public void Send(BrokeredMessage message)
        {
            try
            {
                SendAsync(message).Wait();
            }
            catch(AggregateException ex)
            {
                ExceptionDispatchInfo.Capture(ex.InnerException).Throw();
                throw;
            }
        }

        public virtual async Task DrainMessagesAsync()
        {
            //while (true)
            //{
              //  var message = await ReceiveAsync().ConfigureAwait(false);
               // if (message == null)
                //    break;
               // await message.CompleteAsync();
               // message.Dispose();
          //  }
        }


  //      public async Task<BrokeredMessage> ReceiveAsync(TimeSpan? timeout = null)
     //   {
    //        if(timeout == null)
       //         timeout = TimeSpan.Zero;
      //      var message = await ReceiveAsyncCore(timeout.Value).ConfigureAwait(false);
       //     return message;
       // }

        private static T ToObject(BrokeredMessage bm)
        {
            if(bm == null)
                return default;
            var result = bm.BodyAsString();
            var obj = Serializer.ToObject<T>(result);
            return obj;
        }

        public ManagementClient GetNamespace()
        {
            return new ManagementClient(ConnectionString);
        }


        public async Task<TDescription> EnsureExistsAsync()
        {
            var retry = 0;
            while(true)
            {
                try
                {

                    var create = TypesMapper.ChangeType<TCreation, TDescription>(Creation);
                    InitDescription(create);

                    var result = default(TDescription);
                    try
                    {
                        result = await GetAsync().ConfigureAwait(false);
                    }
                    catch(MessagingEntityNotFoundException)
                    {
                    }
                    if(result == null)
                    {
                        result = await CreateAsync(create).ConfigureAwait(false);
                    }
        
                    return result;
                }
                catch(MessageLockLostException ex)
                {
                    if(retry == 3)
                        throw;
                    if(!ex.IsTransient)
                        throw;
                    retry++;
                }
            }
        }


        protected abstract Task<TDescription> CreateAsync(TDescription description);
        protected abstract Task<TDescription> GetAsync();

        protected abstract void InitDescription(TDescription description);

    }
}
