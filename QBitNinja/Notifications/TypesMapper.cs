using System;
using System.IO;
using System.Runtime.Serialization;
using Microsoft.Azure.ServiceBus.Management;

namespace QBitNinja.Notifications
{
    public static class TypesMapper
    {

        private static object TopicCreationToTopicDescription(TopicCreation topicCreation)
        {
            return new TopicDescription(topicCreation.Path)
            {
            };
        }
        
        private static object QueueCreationToQueueDescription(QueueCreation queueCreation)
        {
            var result = new QueueDescription(queueCreation.Path)
            {
                
                
            };

            if (queueCreation.RequiresDuplicateDetection != null)
                result.RequiresDuplicateDetection = queueCreation.RequiresDuplicateDetection.Value;

            return result;
        }
        
        private static object SubscriptionCreationToSubscriptionDescription(SubscriptionCreation subscriptionCreation)
        {
            var result = new SubscriptionDescription(subscriptionCreation.TopicPath, subscriptionCreation.Name);
            {
                
                
            };

            
            Console.WriteLine($"{subscriptionCreation.TopicPath} -> {subscriptionCreation.Name}");

        //    if (queueCreation.RequiresDuplicateDetection != null)
         //       result.RequiresDuplicateDetection = queueCreation.RequiresDuplicateDetection.Value;

            return result;
        }
        
        public static T2 ChangeType<T1, T2>(T1 input)
        {
            if (typeof(T1) == typeof(TopicCreation) && typeof(T2) == typeof(TopicDescription))
            {
                var result =(TopicCreationToTopicDescription( input as TopicCreation));
                return (T2) result;
            }
            
            if (typeof(T1) == typeof(QueueCreation) && typeof(T2) == typeof(QueueDescription))
            {
                var result =(QueueCreationToQueueDescription( input as QueueCreation));
                return (T2) result;
            }
            
            if (typeof(T1) == typeof(SubscriptionCreation) && typeof(T2) == typeof(SubscriptionDescription))
            {
                var result =(SubscriptionCreationToSubscriptionDescription( input as SubscriptionCreation));
                return (T2) result;
            }

            
            throw new Exception($"Not supported {typeof(T1)} -> {typeof(T2)}");
        }
    }
}