﻿using NBitcoin;
using NBitcoin.Indexer;
using NBitcoin.Indexer.IndexTasks;
using NBitcoin.Protocol;
using NBitcoin.Protocol.Behaviors;
using QBitNinja.Models;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace QBitNinja.Notifications
{
    public class QBitNinjaNodeListener : IDisposable
    {
        private class Behavior : NodeBehavior
        {
            private readonly QBitNinjaNodeListener _listener;
            public Behavior(QBitNinjaNodeListener listener)
            {
                _listener = listener;
            }

            protected override void AttachCore()
            {
                AttachedNode.StateChanged += AttachedNode_StateChanged;
            }

            private void AttachedNode_StateChanged(Node node, NodeState oldState)
            {
                ListenerTrace.Info("State change " + node.State);
                if(node.State == NodeState.HandShaked)
                {
                    ListenerTrace.Info("Node handshaked");
                    AttachedNode.MessageReceived += _listener.node_MessageReceived;
                    AttachedNode.Disconnected += AttachedNode_Disconnected;
                    ListenerTrace.Info("Connection count : " + NodesGroup.GetNodeGroup(node).ConnectedNodes.Count);
                }
            }

            void AttachedNode_Disconnected(Node node)
            {
                ListenerTrace.Info("Node Connection dropped : " + ToString(node.DisconnectReason));
            }

            private string ToString(NodeDisconnectReason reason)
            {
                if(reason == null)
                    return null;
                return Utils.ExceptionToString(reason.Exception);
            }

            protected override void DetachCore()
            {
                AttachedNode.StateChanged -= AttachedNode_StateChanged;
                AttachedNode.MessageReceived -= _listener.node_MessageReceived;
            }

            public override object Clone()
            {
                return new Behavior(_listener);
            }
        }
        SubscriptionCollection _subscriptions;
        readonly MyLockSlim _subscriptionSlimLock = new MyLockSlim();

        private WalletRuleEntryCollection _wallets;
        readonly MyLockSlim _walletsSlimLock = new MyLockSlim();

        readonly object _lockBalance = new object();
        readonly object _lockTransactions = new object();
        readonly object _lockBlocks = new object();
        readonly object _lockWallets = new object();
        readonly object _lockSubscriptions = new object();

        public QBitNinjaConfiguration Configuration { get; private set; }

        public QBitNinjaNodeListener(QBitNinjaConfiguration configuration)
        {
            Configuration = configuration;
        }

        public AzureIndexer Indexer { get; private set; }


        CustomThreadPoolTaskScheduler _indexerScheduler;

        readonly List<IDisposable> _disposables = new List<IDisposable>();
        public async Task ListenAsync(ConcurrentChain chain = null)
        {
            ListenerTrace.Info($"Connecting to node {Configuration.Indexer.Node}");

            if (Configuration.Indexer.Node == null)
                Configuration.Indexer.Node = "127.0.0.1:" + Configuration.Indexer.Network.DefaultPort;
            
            var ip = IPEndPoint.Parse(Configuration.Indexer.Node);
           
            ListenerTrace.Info($"Connecting to node ip {ip.ToEndpointString()}");
            var node = Node.Connect(Configuration.Indexer.Network, ip);
            ListenerTrace.Info($"Connected, trying handshake...");
            node.VersionHandshake();
            ListenerTrace.Info($"Hanshaked");
            node.Disconnect();

            _chain = new ConcurrentChain(Configuration.Indexer.Network);
            Indexer = Configuration.Indexer.CreateIndexer();
            if(chain == null)
            {
                chain = new ConcurrentChain(Configuration.Indexer.Network);
            }
            _chain = chain;
            ListenerTrace.Info("Fetching headers from " + _chain.Tip.Height + " (from azure)");
            var client = Configuration.Indexer.CreateIndexerClient();
            client.SynchronizeChain(chain, default(CancellationToken)).GetAwaiter().GetResult();
            ListenerTrace.Info("Headers fetched tip " + _chain.Tip.Height);

            _disposables.Add(_indexerScheduler = new CustomThreadPoolTaskScheduler(50, 100, "Indexing Threads"));
            Indexer.TaskScheduler = _indexerScheduler;

            _Group = new NodesGroup(Configuration.Indexer.Network);
            _disposables.Add(_Group);
            _Group.AllowSameGroup = true;
            _Group.MaximumNodeConnection = 2;
            var addressManager = new AddressManager();

            
            await addressManager.AddAsync(ip, IPAddress.Parse("127.0.0.1"));

            _Group.NodeConnectionParameters.TemplateBehaviors.Add(new AddressManagerBehavior(addressManager) { Mode = AddressManagerBehaviorMode.None });
            _Group.NodeConnectionParameters.TemplateBehaviors.Add(new ChainBehavior(_chain) { SkipPoWCheck = true });
            _Group.NodeConnectionParameters.TemplateBehaviors.Add(new Behavior(this));



            ListenerTrace.Info("Fetching wallet rules...");
            _wallets = Configuration.Indexer.CreateIndexerClient().GetAllWalletRules();
            ListenerTrace.Info("Wallet rules fetched");

            ListenerTrace.Info("Fetching wallet subscriptions...");
            _subscriptions =  await SubscriptionCollection.CreateAsync(Configuration.GetSubscriptionsTable().ReadAsync());
            ListenerTrace.Info("Subscriptions fetched");
            _Group.Connect();

            ListenerTrace.Info("Fetching transactions to broadcast...");

            _disposables.Add(
                Configuration
                .Topics
                .BroadcastedTransactions
                .CreateConsumer("listener", true, Configuration.UniquePrefix)
                .EnsureSubscriptionExists()
                .OnMessageAsync(async (tx, ctl) =>
                {
                    uint256 hash = null;
                    var repo = Configuration.Indexer.CreateIndexerClient();
                    var rejects = Configuration.GetRejectTable();
                    try
                    {
                        hash = tx.Transaction.GetHash();
                        var indexedTx = repo.GetTransaction(hash);
                        ListenerTrace.Info("Broadcasting " + hash);
                        var reject = await rejects.ReadOneAsync(hash.ToString());
                        if(reject != null)
                        {
                            ListenerTrace.Info("Abort broadcasting of rejected");
                            return;
                        }

                        if(_broadcasting.Count > 1000)
                            _broadcasting.Clear();

                        _broadcasting.TryAdd(hash, tx.Transaction);
                        if(indexedTx == null || !indexedTx.BlockIds.Any(id => Chain.Contains(id)))
                        {
                            var unused = SendMessageAsync(tx.Transaction);
                        }
                        var reschedule = new[]
                        {
                            TimeSpan.FromMinutes(5),
                            TimeSpan.FromMinutes(10),
                            TimeSpan.FromHours(1),
                            TimeSpan.FromHours(6),
                            TimeSpan.FromHours(24),
                        };
                        if(tx.Tried <= reschedule.Length - 1)
                        {
                            ctl.RescheduleIn(reschedule[tx.Tried]);
                            tx.Tried++;
                        }
                    }
                    catch(Exception ex)
                    {
                        if(!_Disposed)
                        {
                            LastException = ex;
                            ListenerTrace.Error("Error for new broadcasted transaction " + hash, ex);
                            throw;
                        }
                    }
                }));
            ListenerTrace.Info("Transactions to broadcast fetched");

            _disposables.Add(Configuration
                .Topics
                .SubscriptionChanges
                .EnsureSubscriptionExists()
                .AddUnhandledExceptionHandler(ExceptionOnMessagePump)
                .OnMessage(c =>
                {

                    using (_subscriptionSlimLock.LockWrite())
                    {
                        if(c.Added)
                            _subscriptions.Add(c.Subscription);
                        else
                            _subscriptions.Remove(c.Subscription.Id); 
                    }

                }));

            _disposables.Add(Configuration
                .Topics
                .SendNotifications
                .AddUnhandledExceptionHandler(ExceptionOnMessagePump)
                .OnMessageAsync((n, act) =>
                {
                    return SendAsync(n, act).ContinueWith(t =>
                    {
                        if(!_Disposed)
                        {
                            if(t.Exception != null)
                            {
                                LastException = t.Exception;
                            }
                        }
                    });
                }));

            _disposables.Add(Configuration
               .Topics
               .AddedAddresses
               .CreateConsumer("updater", true, Configuration.UniquePrefix)
               .EnsureSubscriptionExists()
               .AddUnhandledExceptionHandler(ExceptionOnMessagePump)
               .OnMessage(evt =>
               {
                   if(evt == null)
                       return;
                   ListenerTrace.Info("New wallet rule");

                   using (_walletsSlimLock.LockWrite())
                   {
                       foreach(var address in evt)
                       {
                           _wallets.Add(address.CreateWalletRuleEntry());
                       } 
                   }

               }));

        }

        void ExceptionOnMessagePump(Exception ex)
        {
            if(!_Disposed)
            {
                ListenerTrace.Error("Error on azure message pumped", ex);
                LastException = ex;
            }
        }

        NodesGroup _Group;
        private async Task SendMessageAsync(Transaction tx)
        {
            var payload = new InvPayload(tx);
            int[] delays = new int[] { 50, 100, 200, 300, 1000, 2000, 3000, 6000, 12000 };
            int i = 0;
            var rpc = Configuration.TryCreateRpcClient();
            var txId = tx.GetHash();
            if(rpc != null)
            {
                try
                {
                    ListenerTrace.Info("Sending " + txId + " via RPC");
                    await rpc.SendRawTransactionAsync(tx);
                    ListenerTrace.Info("Successfully broadcasted");
                    Transaction unused;
                    _broadcasting.TryRemove(txId, out unused);
                }
                catch(NBitcoin.RPC.RPCException ex)
                {
                    ListenerTrace.Info("Broadcasted transaction rejected (" + ex.Message + ") " + txId);
                    var reject = new RejectPayload()
                    {
                        Message = "tx",
                        Reason = ex.Message,
                        Code = RejectCode.INVALID,
                        Hash = txId
                    };
                    Transaction unused;
                    _broadcasting.TryRemove(txId, out unused);
                }
                return;
            }
            while(_Group.ConnectedNodes.Count != 2)
            {
                i++;
                i = Math.Min(i, delays.Length - 1);
                await Task.Delay(delays[i]).ConfigureAwait(false);
            }
            await _Group.ConnectedNodes.First().SendMessageAsync(payload).ConfigureAwait(false);
        }
        private async Task SendAsync(Notify notify, MessageControl act)
        {
            var n = notify.Notification;
            HttpClient http = new HttpClient();
            var message = new HttpRequestMessage(HttpMethod.Post, n.Subscription.Url);
            n.Tried++;
            var content = new StringContent(n.ToString(), Encoding.UTF8, "application/json");
            message.Content = content;
            CancellationTokenSource tcs = new CancellationTokenSource();
            tcs.CancelAfter(10000);

            var subscription = await Configuration.GetSubscriptionsTable().ReadOneAsync(n.Subscription.Id).ConfigureAwait(false);
            if(subscription == null)
                return;

            bool failed = false;
            try
            {
                var response = await http.SendAsync(message, tcs.Token).ConfigureAwait(false);
                failed = !response.IsSuccessStatusCode;
            }
            catch
            {
                failed = true;
            }
            var tries = new[]
            {
                TimeSpan.FromSeconds(0.0),
                TimeSpan.FromMinutes(1.0),
                TimeSpan.FromMinutes(5.0),
                TimeSpan.FromMinutes(30.0),
                TimeSpan.FromMinutes(60.0),
                TimeSpan.FromHours(7.0),
                TimeSpan.FromHours(14.0),
                TimeSpan.FromHours(24.0),
                TimeSpan.FromHours(24.0),
                TimeSpan.FromHours(24.0),
                TimeSpan.FromHours(24.0),
                TimeSpan.FromHours(24.0)
            };

            if(!notify.SendAndForget && failed && (n.Tried - 1) <= tries.Length - 1)
            {
                var wait = tries[n.Tried - 1];
                act.RescheduleIn(wait);
            }
        }

        void TryLock(object obj, Action act)
        {
            if(Monitor.TryEnter(obj))
            {
                try
                {
                    act();
                }
                finally
                {
                    Monitor.Exit(obj);
                }
            }
        }


        private ConcurrentChain _chain;
        public ConcurrentChain Chain
        {
            get
            {
                return _chain;
            }
        }

        public int WalletAddressCount
        {
            get
            {
                _walletsSlimLock.EnterReadLock();
                try
                {
                    return _wallets.Count;
                }
                finally
                {
                    _walletsSlimLock.ExitReadLock();
                }
            }
        }

        readonly ConcurrentDictionary<uint256, Transaction> _broadcasting = new ConcurrentDictionary<uint256, Transaction>();
        readonly ConcurrentDictionary<uint256, uint256> _knownInvs = new ConcurrentDictionary<uint256, uint256>();

        uint256 _LastChainTip;

        void node_MessageReceived(Node node, IncomingMessage message)
        {
            if(_knownInvs.Count == 1000)
                _knownInvs.Clear();
            if(message.Message.Payload is InvPayload)
            {
                var inv = (InvPayload)message.Message.Payload;
                foreach(var inventory in inv.Inventory)
                {
                    Transaction tx;
                    if(_broadcasting.TryRemove(inventory.Hash, out tx))
                        ListenerTrace.Info("Broadcasted reached mempool " + inventory);
                }
                var getdata = new GetDataPayload(inv.Inventory.Where(i => i.Type == InventoryType.MSG_TX && _knownInvs.TryAdd(i.Hash, i.Hash)).ToArray());
                foreach(var data in getdata.Inventory)
                {
                    data.Type = node.AddSupportedOptions(InventoryType.MSG_TX);
                }
                if(getdata.Inventory.Count > 0)
                    node.SendMessageAsync(getdata);
            }
            if(message.Message.Payload is TxPayload)
            {
                var tx = ((TxPayload)message.Message.Payload).Object;
                ListenerTrace.Verbose("Received Transaction " + tx.GetHash());
                Indexer.IndexAsync(new TransactionEntry.Entity(tx.GetHash(), tx, null))
                        .ContinueWith(HandleException);
                Indexer.IndexOrderedBalanceAsync(tx)
                    .ContinueWith(HandleException);
                Async(() =>
                {
                    var txId = tx.GetHash();
                    List<OrderedBalanceChange> balances;
                    
                    
                    _walletsSlimLock.EnterReadLock();
                    try
                    {
                        balances =
                            OrderedBalanceChange
                                .ExtractWalletBalances(txId, tx, null, null, int.MaxValue, _wallets)
                                .AsEnumerable()
                                .ToList();
                    }
                    finally
                    {
                        _walletsSlimLock.ExitReadLock();
                    }

                    UpdateHDState(balances).GetAwaiter().GetResult();

                    Indexer.IndexAsync(balances)
                        .ContinueWith(HandleException);


                    Task notify = null;
                    
                    
                    _subscriptionSlimLock.EnterReadLock();
                    try
                    {
                        var topic = Configuration.Topics.SendNotifications;

                        notify = Task.WhenAll(_subscriptions
                            .GetNewTransactions()
                            .Select(t => topic.AddAsync(new Notify()
                            {
                                SendAndForget = true,
                                Notification = new Notification()
                                {
                                    Subscription = t,
                                    Data = new NewTransactionNotificationData()
                                    {
                                        TransactionId = txId
                                    }
                                }
                            })).ToArray());
                    }
                    finally
                    {
                        _subscriptionSlimLock.ExitReadLock();
                    }
                    notify.Wait();
                });
                var unused = Configuration.Topics.NewTransactions.AddAsync(tx)
                                .ContinueWith(HandleException);
            }

            if(message.Message.Payload is BlockPayload)
            {
                var block = ((BlockPayload)message.Message.Payload).Object;
                var blockId = block.GetHash();

                List<OrderedBalanceChange> balances = null;
                
                _walletsSlimLock.EnterReadLock();
                try
                {

                }
                finally
                {
                    _walletsSlimLock.ExitReadLock();
                }
                
                using(_walletsSlimLock.LockRead())
                {
                    balances = block.Transactions.SelectMany(tx => OrderedBalanceChange.ExtractWalletBalances(null, tx, null, null, 0, _wallets)).ToList();
                }
                UpdateHDState(balances).GetAwaiter().GetResult();
            }

            if(message.Message.Payload is HeadersPayload)
            {
                if(_chain.Tip.HashBlock != _LastChainTip)
                {
                    var header = _chain.Tip.Header;
                    _LastChainTip = _chain.Tip.HashBlock;

                    Configuration.Indexer.CreateIndexer().IndexChain(_chain, default(CancellationToken)).GetAwaiter().GetResult();

                    Async(() =>
                    {
                        CancellationTokenSource cancel = new CancellationTokenSource(TimeSpan.FromMinutes(30));
                        var repo = new CacheBlocksRepository(new NodeBlocksRepository(node));
                        TryLock(_lockBlocks, () =>
                        {
                            new IndexBlocksTask(Configuration.Indexer)
                            {
                                EnsureIsSetup = false,

                            }.Index(new BlockFetcher(Indexer.GetCheckpoint(IndexerCheckpoints.Blocks), repo, _chain)
                            {
                                CancellationToken = cancel.Token
                            }, Indexer.TaskScheduler);
                        });
                        TryLock(_lockTransactions, () =>
                        {
                            new IndexTransactionsTask(Configuration.Indexer)
                            {
                                EnsureIsSetup = false
                            }
                            .Index(new BlockFetcher(Indexer.GetCheckpoint(IndexerCheckpoints.Transactions), repo, _chain)
                            {
                                CancellationToken = cancel.Token
                            }, Indexer.TaskScheduler);
                        });
                        TryLock(_lockWallets, () =>
                        {
                            using(_walletsSlimLock.LockRead())
                            {
                                new IndexBalanceTask(Configuration.Indexer, _wallets)
                                {
                                    EnsureIsSetup = false
                                }
                                .Index(new BlockFetcher(Indexer.GetCheckpoint(IndexerCheckpoints.Wallets), repo, _chain)
                                {
                                    CancellationToken = cancel.Token
                                }, Indexer.TaskScheduler);
                            }
                        });
                        TryLock(_lockBalance, () =>
                        {
                            new IndexBalanceTask(Configuration.Indexer, null)
                            {
                                EnsureIsSetup = false
                            }.Index(new BlockFetcher(Indexer.GetCheckpoint(IndexerCheckpoints.Balances), repo, _chain)
                            {
                                CancellationToken = cancel.Token
                            }, Indexer.TaskScheduler);
                        });
                        TryLock(_lockSubscriptions, () =>
                        {
                            using(_subscriptionSlimLock.LockRead())
                            {
                                new IndexNotificationsTask(Configuration, _subscriptions)
                                {
                                    EnsureIsSetup = false,
                                }
                                .Index(new BlockFetcher(Indexer.GetCheckpointRepository().GetCheckpoint("subscriptions"), repo, _chain)
                                {
                                    CancellationToken = cancel.Token
                                }, Indexer.TaskScheduler);
                            }
                        });
                        cancel.Dispose();
                        var unused = Configuration.Topics.NewBlocks.AddAsync(header);
                    });
                }
            }
            if(message.Message.Payload is GetDataPayload)
            {
                var getData = (GetDataPayload)message.Message.Payload;
                foreach(var data in getData.Inventory)
                {
                    Transaction tx = null;
                    if(data.Type == InventoryType.MSG_TX && _broadcasting.TryGetValue(data.Hash, out tx))
                    {
                        var payload = new TxPayload(tx);
                        node.SendMessageAsync(payload);
                        ListenerTrace.Info("Broadcasted " + data.Hash);
                    }
                }
            }
            if(message.Message.Payload is RejectPayload)
            {
                var reject = (RejectPayload)message.Message.Payload;
                uint256 txId = reject.Hash;
                if(txId != null)
                {
                    ListenerTrace.Info("Broadcasted transaction rejected (" + reject.Code + ") " + txId);
                    if(reject.Code != RejectCode.DUPLICATE)
                    {
                        Configuration.GetRejectTable().Create(txId.ToString(), reject);
                    }
                    Transaction tx;
                    _broadcasting.TryRemove(txId, out tx);
                }
            }
        }

        private async Task UpdateHDState(List<OrderedBalanceChange> balances)
        {
            foreach(var balance in balances)
            {
                await UpdateHDState(balance);
            }

        }

        private async Task UpdateHDState(OrderedBalanceChange entry)
        {
            var repo = Configuration.CreateWalletRepository();
            IDisposable walletLock = null;
            try
            {
                foreach(var matchedAddress in entry.MatchedRules.Select(m => WalletAddress.TryParse(m.Rule.CustomData)).Where(m => m != null))
                {
                    if(matchedAddress.HDKeySet == null)
                        return;
                    var keySet = await repo.GetKeySetDataAsync(matchedAddress.WalletName, matchedAddress.HDKeySet.Name);
                    if(keySet == null)
                        return;
                    var keyIndex = (int)matchedAddress.HDKey.Path.Indexes.Last();
                    if(keyIndex < keySet.State.NextUnused)
                        return;
                    walletLock = walletLock ?? _walletsSlimLock.LockWrite();
                    foreach(var address in await repo.Scan(matchedAddress.WalletName, keySet, keyIndex + 1, 20))
                    {
                        ListenerTrace.Info("New wallet rule");
                        var walletEntry = address.CreateWalletRuleEntry();
                        _wallets.Add(walletEntry);
                    }
                }
            }
            finally
            {
                if(walletLock != null)
                    walletLock.Dispose();
            }
        }

        Task Async(Action act)
        {
            var t = new Task(() =>
            {
                try
                {
                    act();
                }
                catch(Exception ex)
                {
                    if(!_Disposed)
                    {
                        ListenerTrace.Error("Error during task.", ex);
                        LastException = ex;
                    }
                }
            });
            t.Start(TaskScheduler.Default);
            return t;
        }

        void HandleException(Task t)
        {
            if(t.IsFaulted)
            {
                if(!_Disposed)
                {
                    ListenerTrace.Error("Error during asynchronous task", t.Exception);
                    LastException = t.Exception;
                }
            }
        }

        public Exception LastException
        {
            get;
            set;
        }

        volatile bool _Disposed;
        #region IDisposable Members

        public void Dispose()
        {
            _Disposed = true;
            foreach(var dispo in _disposables)
                dispo.Dispose();
            _disposables.Clear();
            if(LastException == null)
                _Finished.SetResult(true);
            else
                _Finished.SetException(LastException);
        }

        #endregion
        TaskCompletionSource<bool> _Finished = new TaskCompletionSource<bool>();
        public Task Running => _Finished.Task;
    }
}
