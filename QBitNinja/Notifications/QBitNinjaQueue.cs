﻿using System;
using System.Threading.Tasks;
using Microsoft.Azure.ServiceBus;
using Microsoft.Azure.ServiceBus.Management;

namespace QBitNinja.Notifications
{
    public class QBitNinjaQueue<T> : QBitNinjaQueueBase<T, QueueCreation, QueueDescription>
    {
        public QBitNinjaQueue(string connectionString, string topic)
            : this(connectionString, new QueueCreation()
            {
                Path = topic
            })
        {

        }
        public QBitNinjaQueue(string connectionString, QueueCreation queue)
            : base(connectionString, queue)
        {
        }


        internal QueueClient CreateQueueClient()
        {
            var client = new QueueClient(ConnectionString, Queue);
            return client;
        }

        public string Queue
        {
            get
            {
                return Creation.Path;
            }
        }


        protected override async Task SendAsync(BrokeredMessage brokered)
        {
            var queue = CreateQueueClient();
            try
            {
                await queue.SendAsync(brokered.TheMessage).ConfigureAwait(false);
            }
            finally
            {
                var unused = queue.CloseAsync();
            }
        }

        protected override IDisposable OnMessageAsyncCore(Func<BrokeredMessage, Task> act, MessageHandlerOptions options)
        {
            var client = CreateQueueClient();

            client.RegisterMessageHandler(async (msg, _) =>
            {
                try
                {
                    var bm = new BrokeredMessage(msg, client);
                    await act(bm).ConfigureAwait(false);

                }
                catch (Exception ex)
                {
                    OnUnHandledException(ex);
                }
                
            }, options);
            
            return new MyActionDisposable(() => client.CloseAsync());
        }

        //protected override Task<BrokeredMessage> ReceiveAsyncCore(TimeSpan timeout)
       // {
       //     return CreateQueueClient().ReceiveAsync(timeout);
       // }


        protected override Task<QueueDescription> CreateAsync(QueueDescription description)
        {
            return GetNamespace().CreateQueueAsync(description);
        }

        protected override Task<QueueDescription> GetAsync()
        {
            return GetNamespace().GetQueueAsync(Queue);
        }

        protected override void InitDescription(QueueDescription description)
        {
            description.Path = Creation.Path;
        }

    }
}
