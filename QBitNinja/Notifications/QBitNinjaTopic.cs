﻿using System;
using System.Runtime.ExceptionServices;
using System.Threading.Tasks;
using Microsoft.Azure.ServiceBus;
using Microsoft.Azure.ServiceBus.Management;
using SubscriptionClient = Microsoft.Azure.ServiceBus.SubscriptionClient;

namespace QBitNinja.Notifications
{

    public class MessageControl
    {

        internal DateTime? Scheduled;

        public void RescheduleIn(TimeSpan delta)
        {
            RescheduleFor(DateTime.UtcNow + delta);
        }

        public void RescheduleFor(DateTime date)
        {
            date = date.ToUniversalTime();
            Scheduled = date;
        }


        public void Complete()
        {
            if(Options.AutoComplete)
                throw new InvalidOperationException("Options.AutoComplete should be true for calling this method");
            _Complete = true;
        }
        internal bool _Complete;


        public MessageHandlerOptions Options
        {
            get;
            set;
        }
    }

    public class QBitNinjaTopic<T> : QBitNinjaQueueBase<T, TopicCreation, TopicDescription>
        where T : class
    {

        class QBitNinjaTopicSubscription : QBitNinjaQueueBase<T, SubscriptionCreation, SubscriptionDescription>
        {
            private readonly TopicCreation _topic;
            public QBitNinjaTopicSubscription(string connectionString, TopicCreation topic, SubscriptionCreation subscription)
                : base(connectionString, subscription)
            {
                _topic = topic;
            }
            protected override Task SendAsync(BrokeredMessage message)
            {
                throw new NotSupportedException();
            }
            

            protected override IDisposable OnMessageAsyncCore(Func<BrokeredMessage, Task> act, MessageHandlerOptions options)
            {
                var client = CreateSubscriptionClient();
                client.RegisterMessageHandler(async (msg, _) =>
                {
                    try
                    {
                        var bm = new BrokeredMessage(msg, client);
                        await act(bm).ConfigureAwait(false);
                    }
                    catch(Exception ex)
                    {
                        OnUnHandledException(ex);
                    }
                    
                }, options);
                
                
     
                return new MyActionDisposable(() => client.CloseAsync());
            }


            private SubscriptionClient CreateSubscriptionClient()
            {
                var client = new SubscriptionClient(ConnectionString, _topic.Path, Creation.Name, ReceiveMode.ReceiveAndDelete);
                return client;
            }


            protected override Task<SubscriptionDescription> CreateAsync(SubscriptionDescription description)
            {
                return GetNamespace().CreateSubscriptionAsync(description);
            }

            protected override Task<SubscriptionDescription> GetAsync()
            {
                return GetNamespace().GetSubscriptionAsync(Creation.TopicPath, Creation.Name);
            }

            protected override void InitDescription(SubscriptionDescription description)
            {
                description.TopicPath = Creation.TopicPath;
                description.SubscriptionName = Creation.Name;
            }


            internal IDisposable OnMessageCoreAsyncPublic(Func<BrokeredMessage, Task> act, MessageHandlerOptions options)
            {
                return OnMessageAsyncCore(act, options);
            }
        }

        public QBitNinjaTopic(string connectionString, string topic, string uniquePrefix)
            : this(connectionString,  new TopicCreation
            {
                Path = topic
            },uniquePrefix)
        {

        }
        public QBitNinjaTopic(string connectionString, TopicCreation topic, string uniquePrefix, SubscriptionCreation defaultSubscription = null)
            : base(connectionString, topic)
        {
            Subscription = defaultSubscription ?? new SubscriptionCreation
            {
                Name = uniquePrefix
            };
            Subscription.TopicPath = topic.Path;
        }


        private SubscriptionCreation Subscription { get; }

        public QBitNinjaTopic<T> CreateConsumer(string subscriptionName, bool machineScope, string uniquePrefix)
        {

            var subscriptionCreation = new SubscriptionCreation
            {
                Name = machineScope ? uniquePrefix + subscriptionName : subscriptionName, 
                TopicPath = Creation.Path,
            };

            subscriptionCreation.Merge(Subscription);
            return new QBitNinjaTopic<T>(ConnectionString, Creation,uniquePrefix, subscriptionCreation);
        }


        private string Topic => Creation.Path;

        private TopicClient CreateTopicClient()
        {
            var client = new TopicClient(ConnectionString, Topic);

            return client;
        }

        protected override async Task SendAsync(BrokeredMessage brokered)
        {
            var topic = CreateTopicClient();
            try
            {
                await topic.SendAsync(brokered.TheMessage).ConfigureAwait(false);
            }
            finally
            {
                await topic.CloseAsync();
            }
        }

        private QBitNinjaTopicSubscription CreateSubscriptionClient()
        {
            return new QBitNinjaTopicSubscription(ConnectionString, Creation, Subscription);
        }

        protected override IDisposable OnMessageAsyncCore(Func<BrokeredMessage, Task> act, MessageHandlerOptions options)
        {
            return CreateSubscriptionClient().OnMessageCoreAsyncPublic(act, options);
        }

        public QBitNinjaTopic<T> EnsureSubscriptionExists()
        {
            try
            {
                CreateSubscriptionClient().EnsureExistsAsync().Wait();
            }
            catch(AggregateException aex)
            {
                ExceptionDispatchInfo.Capture(aex.InnerException).Throw();
            }
            return this;
        }

        protected override Task<TopicDescription> CreateAsync(TopicDescription description)
        {
            return GetNamespace().CreateTopicAsync(description);
        }

        protected override Task<TopicDescription> GetAsync()
        {
            return GetNamespace().GetTopicAsync(Topic);
        }

        protected override void InitDescription(TopicDescription description)
        {
            description.Path = Topic;
        }

    }
}