﻿using Microsoft.WindowsAzure.Storage.Table;
using NBitcoin;
using NBitcoin.Indexer;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace QBitNinja
{
    /// <summary>
    /// Such table can store data keyed by Height/BlockId/TransactionId, and range query them 
    /// </summary>
    public class ChainTable<T>
    {
        public ChainTable(CloudTable cloudTable)
        {
            Table = cloudTable ?? throw new ArgumentNullException("cloudTable");
        }

        public CloudTable Table { get; }

        public Scope Scope
        {
            get;
            set;
        }

        public async Task CreateAsync(ConfirmedBalanceLocator locator, T item)
        {
            var str = Serializer.ToString(item);
            var entity = new DynamicTableEntity(Escape(Scope), Escape(locator));
            PutData(entity, str);
            await Table.ExecuteAsync(TableOperation.InsertOrReplace(entity));
        }



        public async Task Delete(ConfirmedBalanceLocator locator)
        {
            var entity = new DynamicTableEntity(Escape(Scope), Escape(locator))
            {
                ETag = "*"
            };
            await Table.ExecuteAsync(TableOperation.Delete(entity));
        }
        public async Task Delete()
        {
            foreach(var entity in await Table.ExecuteQueryAsync(new TableQuery<DynamicTableEntity>
            {
                FilterString = TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, Escape(Scope))
            }))
            {
                await Table.ExecuteAsync(TableOperation.Delete(entity));
            }
        }

        public async IAsyncEnumerable<T> QueryAsync(ChainBase chain, BalanceQuery query = null)
        {
            if (query == null)
                query = new BalanceQuery();
            var tableQuery = query.CreateTableQuery(Escape(Scope), "");


            await foreach (var itm in ExecuteBalanceQueryAsync(Table, tableQuery, query.PageSizes))
            {

                if (chain.Contains(((ConfirmedBalanceLocator) UnEscapeLocator(itm.RowKey)).BlockHash))
                    yield return Serializer.ToObject<T>(ParseData(itm));

            }

        }

        private string ParseData(DynamicTableEntity entity)
        {
            int i = 0;
            StringBuilder builder = new StringBuilder();
            while(true)
            {
                string name = i == 0 ? "data" : "data" + i;
                if(!entity.Properties.ContainsKey(name))
                    break;
                builder.Append(entity.Properties[name].StringValue);
                i++;
            }
            return builder.ToString();
        }
        private void PutData(DynamicTableEntity entity, string str)
        {
            int i = 0;
            foreach(var part in Split(str, 30000))
            {
                string name = i == 0 ? "data" : "data" + i;
                entity.Properties.Add(name, new EntityProperty(part));
                i++;
            }
        }

        private IEnumerable<string> Split(string str, int charCount)
        {
            int index = 0;
            while(index != str.Length)
            {
                var count = Math.Min(charCount, str.Length - index);
                yield return str.Substring(index, count);
                index += count;
            }
        }

        private async IAsyncEnumerable<DynamicTableEntity> ExecuteBalanceQueryAsync(CloudTable table, TableQuery<DynamicTableEntity> tableQuery, IEnumerable<int> pages)
        {
            pages ??= new int[0];
            using var pagesEnumerator = pages.GetEnumerator();
            
            TableContinuationToken continuation = null;
            do
            {
                tableQuery.TakeCount = pagesEnumerator.MoveNext() ? (int?)pagesEnumerator.Current : null;
                var segment = await table.ExecuteQuerySegmentedAsync(tableQuery, continuation);
                continuation = segment.ContinuationToken;
                
                foreach(var entity in segment)
                {
                    yield return entity;
                }
            } while(continuation != null);
        }

        private static string Escape(ConfirmedBalanceLocator locator)
        {
            locator = Normalize(locator);
            return "-" + locator.ToString(true);
        }

        private static BalanceLocator UnEscapeLocator(string str)
        {
            return BalanceLocator.Parse(str.Substring(1), true);
        }

        private static ConfirmedBalanceLocator Normalize(ConfirmedBalanceLocator locator)
        {
            locator = new ConfirmedBalanceLocator(locator.Height, locator.BlockHash ?? new uint256(0), locator.TransactionId ?? new uint256(0));
            return locator;
        }

        private static string Escape(string scope)
        {
            var result = FastEncoder.Instance.EncodeData(Encoding.UTF8.GetBytes(scope));
            return result;
        }

        private static string Escape(Scope scope)
        {
            return Escape(scope.ToString());
        }
    }
}
