﻿using Newtonsoft.Json;
using QBitNinja.Client.JsonConverters;

namespace QBitNinja.Models

{    
    [JsonConverter(typeof(EnumAliasJsonConverter))]
    public enum SubscriptionType
    {
        [EnumAlias("new-block")]
        NewBlock,
        [EnumAlias("new-transaction")]
        NewTransaction
    }

    [JsonConverter(typeof(EnumTypeJsonConverter))]
    [EnumType(SubscriptionType.NewBlock, typeof(NewBlockSubscription))]
    [EnumType(SubscriptionType.NewTransaction, typeof(NewTransactionSubscription))]
    public class Subscription
    {
        public string Id
        {
            get;
            set;
        }
        public string Url
        {
            get;
            set;
        }
        public SubscriptionType Type
        {
            get;
            set;
        }
    }
}
