﻿using Newtonsoft.Json;
using QBitNinja.Client.JsonConverters;

namespace QBitNinja.Models
{

    [JsonConverter(typeof(EnumTypeJsonConverter))]
    [EnumType(SubscriptionType.NewBlock, typeof(NewBlockNotificationData))]
    [EnumType(SubscriptionType.NewTransaction, typeof(NewTransactionNotificationData))]
    public class NotificationData
    {
        public SubscriptionType Type
        {
            get;
            set;
        }
    }
}
