using System;
using System.Threading;

namespace QBitNinja
{
    
    public class MyLockSlim 
    {
        
        private readonly ReaderWriterLockSlim _readerWriterLockSlim = new ReaderWriterLockSlim();

        public IDisposable LockRead()
        {
            _readerWriterLockSlim.EnterReadLock();
            return new MyActionDisposable(()=>_readerWriterLockSlim.ExitReadLock());
        }
        
        public IDisposable LockWrite()
        {
            _readerWriterLockSlim.EnterReadLock();
            return new MyActionDisposable(()=>_readerWriterLockSlim.ExitWriteLock());
        }


        public void EnterReadLock()
        {
            _readerWriterLockSlim.EnterReadLock();
        }

        public void ExitReadLock()
        {
            _readerWriterLockSlim.ExitReadLock();
        }

    }
    
    
    
}