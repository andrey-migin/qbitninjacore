﻿using NBitcoin;
using System;
using System.Diagnostics;


namespace QBitNinja
{
    public static class ListenerTrace
    {
        static readonly TraceSource Source = new TraceSource("QBitNinja.Listener");
        public static void Error(string info, Exception ex)
        {
            Console.WriteLine("Error: "+info);
            Source.TraceEvent(TraceEventType.Error, 0, info + "\r\n" + Utils.ExceptionToString(ex));
        }
        public static void Info(string info)
        {
            Console.WriteLine(info);
            Source.TraceInformation(info);
        }

        internal static void Verbose(string info)
        {
            Console.WriteLine("Verbose: "+info);
            Source.TraceEvent(TraceEventType.Verbose, 0, info);
        }
    }
}
