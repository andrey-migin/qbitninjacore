﻿using Microsoft.Extensions.Configuration;
using Microsoft.WindowsAzure.Storage.Table;
using NBitcoin.Indexer;
using NBitcoin.Protocol;
using QBitNinja.Models;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.IO;
using NBitcoin.RPC;

namespace QBitNinja
{

	
	public class QBitNinjaConfiguration
	{
		public QBitNinjaConfiguration()
		{
			CoinbaseMaturity = 100;
		}

		public string UniquePrefix { get; private set; }
		public static QBitNinjaConfiguration FromConfiguration(IConfiguration configuration)
		{

			
            // Make sure the networks are registered
            NBitcoin.Altcoins.AltNetworkSets.GetAll().Select(c => c.Regtest).ToList();
			var conf = new QBitNinjaConfiguration
			{
				Indexer = IndexerConfiguration.FromConfiguration(configuration),
				LocalChain = configuration["LocalChain"],
				ServiceBus = configuration["ServiceBus"],
				RpcConnectionString = configuration["RPCConnectionString"],
				UniquePrefix = configuration["ConsumerUniquePrefix"]
			};
			
			
			if (string.IsNullOrEmpty(conf.UniquePrefix))
				throw new Exception("Please specify ConsumerUniquePrefix in Yaml");
			
			var nocache = configuration["NoLocalChain"] == "true";
			var qbitDirectory = Environment.GetEnvironmentVariable("HOME");
			if(string.IsNullOrEmpty(conf.LocalChain))
			{
				conf.LocalChain = Path.Combine(qbitDirectory, "LocalChain.dat");
			}
			if(nocache)
				conf.LocalChain = null;
			conf.CoinbaseMaturity = conf.Indexer.Network.Consensus.CoinbaseMaturity;
			return conf;
		}

		public IndexerConfiguration Indexer
		{
			get;
			set;
		}

		public string LocalChain
		{
			get;
			set;
		}

		public async Task EnsureSetupAsync()
		{

			await GetCallbackTable().CreateIfNotExistsAsync();
			await GetChainCacheCloudTable().CreateIfNotExistsAsync();
			await GetCrudTable().CreateIfNotExistsAsync();
			await GetRejectTable().Table.CreateIfNotExistsAsync();
			await GetSubscriptionsTable().Table.CreateIfNotExistsAsync();

			await Indexer.EnsureSetupAsync();
			await Topics.EnsureSetupAsync();
		}



		public CrudTable<Subscription> GetSubscriptionsTable()
		{
			return GetCrudTableFactory().GetTable<Subscription>("subscriptions");
		}

		public CrudTable<RejectPayload> GetRejectTable()
		{
			return GetCrudTableFactory().GetTable<RejectPayload>("rejectedbroadcasted");
		}


		private QBitTopics _topics;
		public QBitTopics Topics => _topics ??= new QBitTopics(this, UniquePrefix);


		public CloudTable GetCallbackTable()
		{
			return Indexer.GetTable("callbacks");
		}

		private CloudTable GetCrudTable()
		{
			return Indexer.GetTable("crudtable");
		}

		private CloudTable GetChainCacheCloudTable()
		{
			return Indexer.GetTable("chainchache");
		}


		//////TODO: These methods will need to be in a "RapidUserConfiguration" that need to know about the user for data isolation (using CrudTable.Scope)

		public CrudTable<T> GetCacheTable<T>(Scope scope = null)
		{
			return GetCrudTableFactory(scope).GetTable<T>("cache");
		}

		private CrudTableFactory GetCrudTableFactory(Scope scope = null)
		{
			return new CrudTableFactory(GetCrudTable, scope);
		}

		public WalletRepository CreateWalletRepository(Scope scope = null)
		{
			return new WalletRepository(
					Indexer.CreateIndexerClient(),
					GetChainCacheTable<BalanceSummary>,
					GetCrudTableFactory(scope));
		}

		private ChainTable<T> GetChainCacheTable<T>(Scope scope)
		{
			return new ChainTable<T>(GetChainCacheCloudTable())
			{
				Scope = scope
			};
		}

		///////

		private int CoinbaseMaturity
		{
			get;
			set;
		}

		public string ServiceBus
		{
			get;
			set;
		}

		private string RpcConnectionString
		{
			get;
			set;
		}

		public RPCClient TryCreateRpcClient()
		{
			if(string.IsNullOrEmpty(RpcConnectionString))
				return null;
			return !RPCCredentialString.TryParse(RpcConnectionString, out var result) 
				? null 
				: new RPCClient(result, Indexer.Network);
		}
	}
}
