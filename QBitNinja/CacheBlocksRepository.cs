﻿using NBitcoin;
using NBitcoin.Indexer.IndexTasks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace QBitNinja
{
    public class CacheBlocksRepository : IBlocksRepository
    {
        private readonly IBlocksRepository _repo;

        public CacheBlocksRepository(IBlocksRepository repo)
        {
            _repo = repo ?? throw new ArgumentNullException("repo");
        }

        private List<Tuple<uint256, Block>> _lastAsked = new List<Tuple<uint256, Block>>();

		#region IBlocksRepository Members

        private const int MaxBlocks = 70;

        public IEnumerable<NBitcoin.Block> GetBlocks(IEnumerable<NBitcoin.uint256> hashes, CancellationToken cancellation)
        {
            var enumerable = hashes as uint256[] ?? hashes.ToArray();
            var asked = enumerable.ToList();
			if(asked.Count > MaxBlocks)
				return _repo.GetBlocks(enumerable, cancellation);
            var lastAsked = _lastAsked;

            if(lastAsked != null && asked.SequenceEqual(lastAsked.Select(a => a.Item1)))
                return lastAsked.Select(l=>l.Item2);
            var blocks = _repo.GetBlocks(enumerable, cancellation).ToList();
            _lastAsked = blocks.Count < 5 ? blocks.Select(b => Tuple.Create(b.GetHash(), b)).ToList() : null;
            return blocks;
        }

        #endregion
    }
}
