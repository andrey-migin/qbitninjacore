﻿using QBitNinja.Notifications;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Runtime.ExceptionServices;

namespace QBitNinja.Listener.Console
{

    class Program
    {
        static void Main(string[] args)
        {
 
            var confReader = new ConfigurationReaderFormYaml();
            var conf = QBitNinjaConfiguration.FromConfiguration(confReader);

            conf.EnsureSetupAsync().Wait();


            List<IDisposable> dispo = new List<IDisposable>();
            List<Task> running = new List<Task>();
            try
            {

                var listener = new QBitNinjaNodeListener(conf);
                dispo.Add(listener);
                listener.ListenAsync().Wait();
                running.Add(listener.Running);

                if (running.Count > 0)
                    Task.WaitAny(running.ToArray());
            }
            finally
            {
                foreach (var d in dispo)
                    d.Dispose();
            }

        }


    }
}
