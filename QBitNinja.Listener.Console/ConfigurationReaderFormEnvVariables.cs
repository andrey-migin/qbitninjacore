using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Primitives;
using MyYamlSettingsParser;

namespace QBitNinja.Listener.Console
{
    public class ConfigurationReaderFormYaml : IConfiguration
    {
        public IConfigurationSection GetSection(string key)
        {
            return null;
        }

        public IEnumerable<IConfigurationSection> GetChildren()
        {
            return Array.Empty<IConfigurationSection>();
        }

        public IChangeToken GetReloadToken()
        {
            return null;
        }


        private List<YamlLine> _lines;

        private void ReadYaml()
        {
            var file = Environment.GetEnvironmentVariable("HOME") + '/' + ".ninja-listener";

            var data = File.ReadAllBytes(file);

            _lines = data.ParseYaml().ToList();

        }

        public string this[string key]
        {
            get
            {
                
                System.Console.WriteLine("Reading Key from Env Variable: "+key);
                if (_lines == null)
                    ReadYaml();


                var keys = key.Split(".");

                if (keys.Length == 1)
                    return _lines.FirstOrDefault(itm => itm.Keys[0] == keys[0] && itm.Keys.Length == 1)?.Value;

                if (keys.Length == 2)
                    return _lines.FirstOrDefault(itm => itm.Keys.Length == 2 && itm.Keys[0] == keys[0] && itm.Keys[1] == keys[1])?.Value;

                return null;

            }
            set => throw new NotImplementedException();
        }
    }
}